/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-format-gzip
//
//    IO module that implements GZIP stream support.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var type =     require("type");
var event =    require("event");
var ioStream = require("io-stream");
var inflate =  require("inflate");

var GZIP_TAG_TYPE_OBJECT = "gzip-tag-type-object";

var ERROR_UNSUPPORTED_STREAM =  "Unsupported Stream";
var ERROR_CORRUPTED_GZIP_FILE = "Corrupted Gzip File";

var T_DEFLATE=  8;
var F_TEXT=     1 << 0;
var F_CRC=      1 << 1;
var F_EXTRA=    1 << 2;
var F_NAME=     1 << 3;
var F_COMMENT=  1 << 4;

var HEADER_SIZE = 10;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////////////////
function readString(header, pos, length) {
    var zeroIdx = pos;
    for (; zeroIdx<header.length; zeroIdx++) {
        if (header[zeroIdx] == 0 || (length && zeroIdx == pos + length)) {
            break;
        }
    }
    if (zeroIdx == header.length && !length) {
        throw("No null byte encountered");
    }
    return header.subarray(pos, zeroIdx);
}

function readInt(header, pos, length) {
    var l = 0;
    var str = readString(header, pos, length);
    if (str != "")
    {
        try
        {
            l = parseInt(str, 8);
        }
        catch(e)
        {
            //not an integer
        }
    }
    return l;
}

function open(stream) {
    var reader = new GZipReader(stream);

    var streamName;
    var streamPosition = 0;
    var objectCount = 0;

    function probe(header)
    {
        return new Promise(function(resolve, reject) {
            var tObj = new GZipObject(header, stream);
            if (!tObj.isValid)
            {
                reject(new Error(ERROR_UNSUPPORTED_STREAM, "The stream does not seem to contain a valid gzip file."));
                return;
            }
            resolve();
        });
    }
    function getStreamName(reader) {
        return stream.getName(); 
    }
    function getStreamLength(name) {
        streamName = name;

        return stream.getLength(); 
    }
    function updateReader(length) {
        return new Promise(function(resolve, reject) {
            reader.file.name = streamName;
            reader.file.size = length;
            resolve();
        });
    }
    function readMetadata() {
        return new Promise(function(resolve, reject) {
            stream.read(HEADER_SIZE, streamPosition)
                .then(processGZipHeader)
                .then(resolve)
                .catch(function handleGZipError(e) { // when implementing multi file support, change "resolve" to "readNext"
                    if (e.name != ioStream.ERROR_INVALID_LENGTH) {
                        reject(e);
                        return;
                    }
                    resolve(reader);
                });
        });
    }
    function processGZipHeader(header) {
        return new Promise(function(resolve, reject) {
            var tT = new GZipTag("object" + objectCount, GZIP_TAG_TYPE_OBJECT, { 
                "header" : header, 
                "stream" : stream 
            });

            objectCount++;
            streamPosition += HEADER_SIZE + tT.size;
            reader.metadata.tags.push(tT);

            resolve();
        });
    }

    return new Promise(function(resolve, reject) {
        stream.read(HEADER_SIZE, 0)
            .then(probe)
            .then(getStreamName)
            .then(getStreamLength)
            .then(updateReader)
            .then(readMetadata)
            .then(resolve)
            .catch(reject);
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// GZipObject Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function GZipObject(header, stream) {
    var self = this;
    this.isValid = false;

    if (!header) {
        return;
    }

    //https://gist.github.com/kig/417483
    //http://www.zlib.org/rfc-gzip.html
    this.id1 = header[0]; //BYTE 0 - BYTE
    this.id2 = header[1]; //BYTE 1 - BYTE
    this.compressionMethod = header[2]; //BYTE 2 - BYTE
    this.flags = header[3]; //BYTE 3 - BYTE
    this.mTime = null; //BYTE 4 - UInt32LE
    this.extraFlags = header[8]; //BYTE 8 - BYTE
    this.operatingSystem = header[9]; //BYTE 9 - BYTE

    var offset = 10;
    //if flags extra
    this.extraField = "";
    if (self.flags & F_EXTRA) {
        var xLen = readInt(header, offset, 2);
        offset += 2;
        self.extraField = header.subarray(offset, offset + xLen); //BYTE 10 -> 11 = XLENGTH - UInt16LE, BYTE 12 -> XLENGTH = FIELD - STRING
        offset += xLen;
    }
    //if flags name
    this.name = "";
    if (self.flags & F_NAME) {
        self.name = readString(header, offset); //BYTE 10 + 2 + XLENGTH + STRINGLENGTH UNTIL 0 BYTE
        offset += self.name.length + 1;
    }
    //if flags comment
    this.comment = "";
    if (self.flags & F_COMMENT) {
        self.comment = readString(header, offset); //BYTE 10 + 2 + XLENGTH + FILENAMELENGTH + 1
        offset += self.comment.length + 1;
    }
    //this.computedCRC16 = null; //CRC16(BYTE 10 + 2 + XLENGTH + FILENAMELENGTH + 1 + COMMENTLENGTH + 1);
    //if flags header checksum
    //this.headerCRC16 = null; //BYTE 10 + 2 + XLENGTH + FILENAMELENGTH + 1 + COMMENTLENGTH + 1 - UInt16LE

    if (self.flags & F_CRC) {
        //    h.headerCRC16 = Bin.UInt16LE(s, offset);
        //    if (h.computedHeaderCRC16 != null && h.headerCRC16 != h.computedHeaderCRC16)
        //        throw("Header CRC16 check failed");
        offset += 2;
    }

    this.offset = offset; //BYTE 10 + 2 + XLENGTH + FILENAMELENGTH + 1 + COMMENTLENGTH + 1 + 2
    this.size = null; //CALCULATE FROM LENGTH

    self.isValid = validate(header);

    function validate(header)
    {
        // check compression method
        if (self.id1 != 0x1f || self.id2 != 0x8b || self.compressionMethod != T_DEFLATE) {
            return false;
        }

        // header checksum
        /*var sum = 0;
         for (var b = 0; b < header.length; b++)
         {
         if (b >= 148 && b < 148 + 8)
         {
         sum += 32;
         }
         else
         {
         sum += header[b];
         }
         }
         return sum == self.checksum;*/
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// GZipReader Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function GZipReader(stream) {
    var HEADER_SIZE = 10;

    this.err = [];
    this.metadata = new GZipMetadata();
    this.file = {
        "name" : options? options.name : null,
        "size" : options? options.size : null
    }

    this.getGZipObjectStream = function(obj)
    {
        return new GZipStream(stream, obj);
    };

    this.close = function()
    {
        stream.close();
    };
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// GZipTag Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function GZipTag(key, type, data) {
    this.key = key;
    this.type = type;
    this.value = convertDataToValue(type, data);
    this.data = data;

    function convertDataToValue() {
        switch(type) {
            case GZIP_TAG_TYPE_OBJECT:
                return new GZipObject(data.header, data.stream);
                break;
            default:
                throw "Not Implemented";
                break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// GZipMetadata Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function GZipMetadata() {
    this.tags = [];
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// GZipStream Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function GZipStream(stream, object) {
    if (!stream || !object) {
        return null;
    }
    var self = this;

    var inflatedBuffer = null;
    var closed = false;

    function inflateStream() {
        return new Promise(function(resolve, reject) {
            stream.read(null, object.offset).then(function(buff) {
                inflatedBuffer = inflate(buff);

                //TODO - After reading verify checksum in footer
                //if (!tObj.checksum) {
                //    refuse(ERROR_CORRUPTED_GZIP_FILE);
                //    return;
                //}
                if (!inflatedBuffer || inflatedBuffer.length == 0) {
                    reject(new Error(ERROR_CORRUPTED_GZIP_FILE, ""));
                }
                else {
                    resolve();
                }
            }, reject);
        });
    }

    this.getName = function() {
        return object.name;
    };
    this.getLength = function() {
        return new Promise(function(resolve, reject) {
            if (closed) {
                reject(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (!inflatedBuffer) {
                inflateStream().then(function() {
                    self.getLength().then(resolve, reject);
                }, reject);
            }
            else if (inflatedBuffer) {
                resolve(inflatedBuffer.length);
            }
            else {
                reject(new Error(ERROR_CORRUPTED_GZIP_FILE, ""));
            }
        });
    };
    this.read = function (len, position)
    {
        if (!position) {
            position = 0;
        }

        return new Promise(function(resolve, reject) {
            if (closed) {
                reject(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (len == null) {
                self.getLength().then(function (length) {
                    doRead(length - position);
                }, function(err) {
                    reject(err);
                });
            }
            else {
                doRead(len);
            }

            function doRead(len) {
                if (!inflatedBuffer) {
                    inflateStream().then(function() {
                        self.read(len, position).then(resolve, reject);
                    }, reject);
                }
                else if (inflatedBuffer) {
                    if (position + len > inflatedBuffer.length) {
                        reject(new Error(ioStream.ERROR_INVALID_LENGTH, ""));
                    }
                    else if (len == inflatedBuffer.length) {
                        resolve(inflatedBuffer);
                    }
                    else {
                        var nBuf = inflatedBuffer.subarray(position, position + len);
                        position += len;
                        resolve(nBuf);
                    }
                }
                else {
                    reject(new Error(ERROR_CORRUPTED_GZIP_FILE, ""));
                }
            }
        });
    };
    this.close = function ()
    {
        return new Promise(function(resolve, refuse) {
            if (closed) {
                refuse(new Error(ioStream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            inflatedBuffer = null;
            closed = true;

            resolve();
        });
    };

    this.events = new event.Emitter(this);
}
GZipStream.prototype = ioStream;

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = {
    "open" : open,
    "GZipReader" : GZipReader,
    "GZipStream" : GZipStream,
    "ERROR_UNSUPPORTED_STREAM" : ERROR_UNSUPPORTED_STREAM,
    "ERROR_CORRUPTED_GZIP_FILE" : ERROR_CORRUPTED_GZIP_FILE
};